#include <include.h>


// test comment
//
// USED JUST FOR TESTING ATM
// Will be fixed and turned into something more sane as time goes on.

int main(int argc, char **argv)
{
	FILE *f = fopen(argv[1], "rb"); 
	if ( f == NULL ) {
		printf("err: Couldn't Open %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}

	fseek(f, 0L, SEEK_END);
	int fsize = ftell(f);
	fseek(f, 0L, SEEK_SET);

	unsigned char *buffer=malloc(fsize);

	fread(buffer, fsize, 1, f);
	fclose(f);

	int pc = 0;

	while (pc < fsize) {
		pc += disassemble_8080(buffer, pc);
	}

	exit(EXIT_SUCCESS);
}
